using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_defeated : MonoBehaviour
{
    public GameObject endMenu;

    public GameObject healthMenu;

    GameObject[] boss;



    void Start()
    {
        endMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        boss = GameObject.FindGameObjectsWithTag("Boss");

        if (boss.Length <= 0)
        {
            //Debug.Log("boss gone");
            endMenu.SetActive(true);
            //Time.timeScale = 0f;
            healthMenu.SetActive(false);
            

        }
        else if(boss.Length >= 1)
        {
            //Debug.Log("boss not gone");
            endMenu.SetActive(false);
            //Time.timeScale = 1f;
            healthMenu.SetActive(true);
        }
    }
  
}
