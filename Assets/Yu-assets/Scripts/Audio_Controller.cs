using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Audio_Controller : MonoBehaviour
{
    private AudioSource AudioPlayer;
    public GameObject ObjectMusic;

    public Slider volumeSlider;



    private float musicVolume = 1f;


    void Start()
    {
        ObjectMusic = GameObject.FindWithTag("GameMusic");
        AudioPlayer = ObjectMusic.GetComponent<AudioSource>();
        musicVolume = PlayerPrefs.GetFloat("volume");
        AudioPlayer.volume = musicVolume; 
        volumeSlider.value = musicVolume;
    }

    
    void Update()
    {
        AudioPlayer.volume = musicVolume;

        PlayerPrefs.SetFloat("volume", musicVolume);
    }

    public void updateVolume(float volume)
    {
        musicVolume = volume;
    }
}
