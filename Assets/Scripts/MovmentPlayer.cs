using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovmentPlayer : MonoBehaviour
{
    [Header("Movment")]
    public float moveSpeed = 5f;

    public Rigidbody rb;
    public Camera cam;

    Vector3 movement;
    Vector3 mousePos;

    [Header("Gun")]
    //public AudioSource audioSource;
    //public AudioClip audio;
    public Transform spawn;

    [Header("Bullets")]
    public GameObject bulletObj;
    public float bulletSpeed = 20;

    //Animation
    // idle 0 , walk 1
    private enum MoveMentState { idle, walk }
    private Animator anim;

    [Header("Health")]
    public int maxHealth = 100;
    public int currentHealth;

    public HealthBar healthBar;

    GameObject[] boss;

    void Start()
    {
        //animation
        anim = GetComponent<Animator>();

        //Health
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);

    }

    void Update()
    {
        MovePlayer();
        PlayerDeath();
        PlayerDeath();
        UpdateAnimationState();

        if (Input.GetButtonDown("Shoot"))
        {
           Shoot();
        }

        boss = GameObject.FindGameObjectsWithTag("Boss");

        if (boss.Length <= 0)
        {
            moveSpeed = 0f;
            
        }
       

    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        Vector3 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.x, lookDir.z) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.Euler(0f, angle, 0f);
        rb.rotation = rotation;
    }

    void MovePlayer()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.z = Input.GetAxisRaw("Vertical");

        mousePos = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.transform.position.y));
    }

    void UpdateAnimationState()
    {
        MoveMentState state;

        if (movement.magnitude > 0.1f) 
        {
            state = MoveMentState.walk;
        }
        else
        {
            state = MoveMentState.idle;
        }

        anim.SetInteger("MovementState", (int)state);
    }

    public void OnTriggerEnter(Collider CapsuleCollider)
    {
        if (CapsuleCollider.gameObject.CompareTag("Dam_Pl"))
        {
            TakeDamage(15);
        }

        // triggery korutyn
        if (CapsuleCollider.gameObject.tag == ("SpeedBoost"))
        {
            Debug.Log("First base");
            StartCoroutine(SpeedBoost());
        }

        if (CapsuleCollider.gameObject.tag == ("DoubleSpeedBoost"))
        {
            Debug.Log("Second base");
            StartCoroutine(DoubleSpeedBoost());
        }

        if(CapsuleCollider.gameObject.tag == ("PowerBoost"))
        {
            StartCoroutine(Enemy.PowerBoost());
        }

        if(CapsuleCollider.gameObject.tag == ("DoublePowerBoost"))
        {
            StartCoroutine(Enemy.DoublePowerBoost());
        }

    }
    void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
    }

    void PlayerDeath()
    {
        if (currentHealth <= 0)
        {
            Debug.Log("Death");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void Shoot()
    {
        Ray ray = new Ray(spawn.position, spawn.forward);
        RaycastHit hit;

        float shotDistance = 20;

        if (Physics.Raycast(ray, out hit, shotDistance))
        {
            var bullet = Instantiate(bulletObj, spawn.position, spawn.rotation);
            bullet.GetComponent<Rigidbody>().velocity = spawn.forward * bulletSpeed;
          
            shotDistance = hit.distance;
        }
        Debug.Log("shoot");
       // AudioSource.PlayClipAtPoint(audio, spawn.transform.localPosition);

    }

    // korutynki
    IEnumerator SpeedBoost()
    {
        //boosting = true;
        moveSpeed = 10f;
        yield return new WaitForSeconds(5f);
        moveSpeed = 5f;

        Debug.Log("Boosting");
    }

    IEnumerator DoubleSpeedBoost()
    {
        moveSpeed = 15f;
        yield return new WaitForSeconds(5f);
        moveSpeed = 5f;

        Debug.Log("Daboosted");
    }

   
}
