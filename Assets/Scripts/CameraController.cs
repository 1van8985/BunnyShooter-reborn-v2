using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject playerObj;
    public Vector3 distance;
    public float lookUp;

    void Start()
    {
    }

    void Update()
    {
        transform.position = playerObj.transform.position + distance;
        transform.LookAt(playerObj.transform.position);
        transform.Rotate(-lookUp, 0, 0);
    }
    
}
