using System.Collections;
using System.Collections.Generic;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour, IDamageable
{
    //health
    private int maxHealth = 100;
    public int currentHealth;

    public HealthBar healthBar;

    public static int damage = 15;

    public void Start()
    {
        healthBar.GetComponent<HealthBar>();
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    void OnTriggerEnter(Collider CapsuleCollider)
    {
        if (CapsuleCollider.gameObject.CompareTag("Dam_En"))
        {
            TakeDamage(damage);
        }

    }

    public void TakeDamage(int damage)
    {
       
 Debug.Log("damage");

        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);

        if (currentHealth <= 0)
        {
            EnemyDeath();
        }
        
       
    }

    void EnemyDeath()
    {
        if (currentHealth <= 0)
        {
            Debug.Log("EnemyDeath");
            Destroy(healthBar);
            Destroy(gameObject);
        }
    }

    public static IEnumerator PowerBoost()
    {
        Debug.Log("Received");
        damage = 20;
        Debug.Log("Damage is 20");
        yield return new WaitForSeconds(5f);
        damage = 15;
        Debug.Log("Damage is 15");
    }

    public static IEnumerator DoublePowerBoost()
    {
        damage = 25;
        yield return new WaitForSeconds(5f);
        damage = 15;
    }
}