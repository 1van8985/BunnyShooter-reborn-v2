using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RunAwayEnemy : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent = null;
    public Transform player;
    public float speed = 5f;

    private float displacementDis = 20;
    RaycastHit hit;

    //Animation
    // idle 0 , walk 1, death 2
    private enum MoveMentState { idle, walk, death }
    private Animator anim;

    void Start()
    {
        if (agent == null)
        {
            if (!TryGetComponent(out agent))
            {
                Debug.LogWarning(name + "needs a NavMesh agent");
            }
        }

        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        UpdateAnimationState();

        Vector3 normDir = (player.position - transform.position).normalized;
        normDir = Quaternion.AngleAxis(45, Vector3.up) * normDir;

        if (Vector3.Distance(transform.position, player.position) < displacementDis)
        {
            if (Physics.Raycast(transform.position, (player.position - transform.position), out hit, displacementDis))
            {
                if (hit.transform == player)
                {
                    MoveToPose(transform.position - (normDir * displacementDis) * speed);
                }
            }
        }
    }

    void UpdateAnimationState()
    {
        MoveMentState state;

        if (agent.velocity.magnitude > 0.1f)
        {
            state = MoveMentState.walk;
        }
        else
        {
            state = MoveMentState.idle;
        }

        anim.SetInteger("MovementState", (int)state);
    }

    private void MoveToPose(Vector3 pos)
    {
        agent.SetDestination(pos);
        agent.isStopped = false;
    }
}
