using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChasingEnemy : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent=null;
    public Transform player;
    public float speed = 5f;

    private float minDistance = 20;
    RaycastHit hit;

    //Animation
    // idle 0 , walk 1, death 2, attack 3
    private enum MoveMentState { idle, walk, death, attack }
    private Animator anim;

    void Start()
    {
        if (agent == null)
        {
            if (!TryGetComponent(out agent))
            {
                Debug.LogWarning(name + "needs a NavMesh agent");
            }
        }

        anim = GetComponent<Animator>();
    }

    void Update()
    {
        UpdateAnimationState();

        Vector3 normDir = (player.position - transform.position).normalized;
        normDir = Quaternion.AngleAxis(45, Vector3.up) * normDir;

        if (Vector3.Distance(transform.position, player.position) < minDistance)
        {
            if (Physics.Raycast(transform.position, (player.position - transform.position), out hit, minDistance))
            {
                if (hit.transform == player)
                {
                    MoveToPlayer(transform.position - (normDir * minDistance) * speed);
                }
            }
        }
    }

    void UpdateAnimationState()
    {
        MoveMentState state;

        if (agent.velocity.magnitude > 0.05f)
        {
            state = MoveMentState.walk;
        }
        else
        {
            state = MoveMentState.idle;
        }

        if (Vector3.Distance(transform.position, player.transform.position) < 2f)
        {
            state = MoveMentState.attack;
        }

        anim.SetInteger("MovementState", (int)state);
    }

    private void MoveToPlayer(Vector3 pos)
    {
        agent.SetDestination(player.position);
        agent.isStopped = false;
    }
}
