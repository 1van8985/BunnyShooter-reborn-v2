using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovmentPlayerLvl2 : MonoBehaviour
{
    //Fix?
    private float degree = 180f;

    /// Movment
    public float moveSpeed = 5f;

    public Rigidbody rb;
    public Camera cam;

    Vector3 movement;
    Vector3 mousePos;

    //Animation
        // idle 0 , walk 1
    private enum MoveMentState { idle, walk }
    private Animator anim;

    //Health
    public int maxHealth = 100;
    public int currentHealth;

    public HealthBar healthBar;

    void Start()
    {
        //animation
        anim = GetComponent<Animator>();

        //Health
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    void Update()
    {
        MovePlayer();
        PlayerDeath();
        UpdateAnimationState();
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        Vector3 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.x, lookDir.z) * Mathf.Rad2Deg - degree;
        Quaternion rotation = Quaternion.Euler(0f, angle, 0f);
        rb.rotation = rotation;
    }

    void MovePlayer()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.z = Input.GetAxisRaw("Vertical");

        mousePos = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.transform.position.y));
    }

    void UpdateAnimationState()
    {
        MoveMentState state;

        if (movement.magnitude > 0.1f) 
        {
            state = MoveMentState.walk;
        }
        else
        {
            state = MoveMentState.idle;
        }

        anim.SetInteger("MovementState", (int)state);
    }

    void OnTriggerEnter(Collider CapsuleCollider)
    {
        if (CapsuleCollider.gameObject.CompareTag("Dam_Pl"))
        {
            TakeDamage(15);
        }

    }
    void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
    }

    void PlayerDeath()
    {
        if (currentHealth <= 0)
        {
            Debug.Log("Death");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
