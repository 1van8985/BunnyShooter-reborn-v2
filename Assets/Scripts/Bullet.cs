using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float life = 3;
    public GameObject bullet;

   
    void Awake()
    {
        Destroy(gameObject, life);
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision == collision)
        {
            Destroy(gameObject);
        }
    }


}
