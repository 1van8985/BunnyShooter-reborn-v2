using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Interaction : MonoBehaviour
{
    [SerializeField] private bool detected = false;

    public GameObject itemMenu;

    public GameObject healthMenu;

    public PauseMenu pauseMenu;

    public void start()
    {
        itemMenu.SetActive(false);
        
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            detected = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            detected = false;
        }
    }

    private void Update()
    {
        if (detected && Input.GetKeyDown(KeyCode.E))
        {
            ReadItem();
        }
        else if (detected && Input.GetKeyDown(KeyCode.Escape))
        {
            itemMenu.SetActive(false);
        }

    }

    public void ReadItem()
    {
        itemMenu.SetActive(true);
        pauseMenu.Pause();
    }
}
